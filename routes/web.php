<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/values', 'FruitController@getFruits')->name('home');
Route::post('/values', 'FruitController@storeFruit')->name('storefruit');
Route::post('/', function () { return 'A'; });