## Important Note ##
1.	Clone the project : git clone https://TropaMXmood@bitbucket.org/TropaMXmood/larafruitsblog.git
2.	$cd project-name
3.	rename .env.example in projectís root directory as .env file
4.	$composer update
5.	$git clone https://github.com/Laradock/laradock.git
6.	$php artisan key:generate
7.	$php artisan config:clear
8.	$cd laradock
9.	rename .env.example file in laradock folder as .env file
10.	$docker-compose up -d nginx mysql phpmyadmin redis
11.	$docker-compose ps (to see all the containers are up or not)
12.	$docker inspect -f '{{.Name}} - {{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $(docker ps -aq)

(to check mysql_host_ip)

13.	go to phpmyadmin in browser :
http://localhost:8080
use below credential:
host: mysql_host_ip (usually 172.20.0.2 if not put ip from the result of above command)
username: root
password: root

14.	create ur database named: larablog
15.	$ docker-compose exec workspace bash
16.	$ php artisan migrate


Postman
GET url : localhost/values or localhost/values?keys=key1,key2
POST url : localhost/values
